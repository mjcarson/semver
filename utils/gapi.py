#! /usr/bin/env python3

import os
import re
import json
import gitlab

class Gapi():
    '''Gitlab api client for semver'''
    def __init__(self, url, token=None, token_type='job', project=None, merge_commits=True):
        '''init Gapi'''
        # project id
        # get project id from environment if not defined
        if project is None:
            self.project_id = os.environ["CI_PROJECT_ID"]
        else:
            self.project_id = project
        
        # init gitlab correctly
        # private token
        if token_type == 'private':
            self.gitlab = gitlab.Gitlab(url, private_token=token)
        # CI job token
        if token_type == 'job':
            if token is None:
                token = os.environ["CI_JOB_TOKEN"]
            self.gitlab = gitlab.Gitlab(url, job_token=token)
        
        # get our project
        self.project = self.gitlab.projects.get(self.project_id)

        # whether to support merge commits or not
        self.merge_commits = merge_commits
   
    def get_mr(self, commit=None, release=False):
        '''
        gets the mr that made this commit

        Args:
            commits (string): current commit sha
        Returns:
            gitlab.v4.objects.ProjectMergeRequest
        '''
        # get our commit from environment if its not passed in
        if commit is None:
            commit = os.environ["CI_COMMIT_SHA"]

        # commits to check
        check_commits = []
        # support merge commits if they are enabled
        if self.merge_commits == True:    
            # get our commits info
            commit_info = self.project.commits.get(commit)
            # check against either parent
            check_commits = check_commits + commit_info.attributes["parent_ids"]
        
        check_commits.append(commit)

        # get all merged merge requests and iterate over them
        mr_list = self.project.mergerequests.list(state="merged", order_by="updated_at")
        for mr in mr_list:
            # make sure we are the target branch
            if os.environ["CI_COMMIT_REF_NAME"] != mr.target_branch:
                continue

            # make sure mr has a label
            if (set(mr.labels).isdisjoint(["patch", 'minor', "major"]) == False 
                    or release == True):
                # get first commit of mr
                mr_commit = next(mr.commits()._list)

                # check if this is our commit
                if mr_commit["id"] in check_commits:
                    return mr
    
    def get_tag(self, commit=None):
        '''
        get tag for current commit if one exists

        Args:
            commits (string, optional): current commit sha
        Returns:
            string: title of tag for this commit
            None: not tagged
        '''
        # get our commit from environment if its not passed in
        if commit is None:
            commit = os.environ["CI_COMMIT_SHA"]

        # iterate over tags until we find our commit
        tag_list = self.project.tags.list()
        for tag in tag_list:
            # check if this is our tag
            if tag.attributes["commit"]["id"] == commit:
                return tag.attributes["name"]

    def latest(self, commit=None, branch=None):
        '''
        check if latest commit in branch

        Args:
            commits (string, optional): current commit sha
        Returns:
            bool: whether this is the latest commit
        '''
        # get our commit from environment if its not passed in
        if commit is None:
            commit = os.environ["CI_COMMIT_SHA"]

        # get our branch name if its passed in
        if branch is None:
            branch = os.environ["CI_COMMIT_REF_NAME"]
        
        # check if we are the latest commit
        if self.project.branches.get(branch).attributes['commit']['id'] == commit:
            return True

        # were not the latest commit
        return False

    def get_bump(self, commit=None):
        '''
        Gets how we should bump the version

        Args:
            commits (string): current commit sha
        Returns:
            string: how to bump the MR (patch, minor, major)
        '''
        # get our mr
        mr = self.get_mr(commit)
        
        # get type of bump
        # patch
        if "patch" in mr.labels:
            return "patch"

        # minor
        elif "minor" in mr.labels:
            return "minor"

        # major
        elif "major" in mr.labels:
            return "major"

    
    def get_last_tag(self, prepend=None, release=False):
        '''
        get last version tag for project with an optional prepended string

        Args:
            prepend (string, optional): prepended string at front of version number
            release (bool, optional): whether to require the commit be a release
        Returns:
            dict: gitlab tag attributes
        '''
        if prepend is None:
            prepend = ""

        # iterate over commits in branch
        for tag in self.project.tags.list():
            # reset possible tag
            possible_tag = None

            # latest tag that does not have to be a release
            if tag.attributes["name"].startswith(prepend) == True and \
                    release == False:
                possible_tag = tag.attributes
            # latest tag that has to be a release
            if tag.attributes["name"].startswith(prepend) == True and \
                    tag.attributes["release"] is not None:
                possible_tag = tag.attributes

            # make sure this is a valid semver tag
            # strip any prepend if one exists
            if possible_tag is not None:
                possible_name = possible_tag["name"].replace(prepend, '')
                if re.match("^[0-9]+\.[0-9]+\.[0-9]+$", possible_name) != None:
                    return possible_tag

    def create_tag(self, title, message=None, release=None, ref=None):
        '''
        Create a tag tied to our current branch

        Args:
            title (string): title of tag
            message (string, optional): message for tag
            release (string, optional): reeleases description
            ref (string, optional): commit id or branch name
        Returns:
           gitlab.v4.objects.ProjectTag 
        '''
        # get our commit if ref isn't passed in
        if ref is None:
            ref = os.environ["CI_COMMIT_SHA"]
        
        # build tag doc
        tag_doc = {'tag_name': title, 'ref': ref}
        # inject message and release if they are supplied
        if message is not None:
            tag_doc["message"] = message
        if release is not None:
            tag_doc["release_description"] = release

        # create tag
        return self.project.tags.create(tag_doc)

    def inject_release_notes(self, tag, release):
        '''
        Adds release notes to a tag

        
        Args:
            tag (string): name of tag to add release notes too
            release (string): release notes to add
        Returns:
            gitlab.v4.objects.ProjectRelease
        '''
        # check if release notes already exist
        try:
            release = self.project.releases.get(tag)
        except gitlab.exceptions.GitlabGetError:
            # create release notes
            release = self.project.releases.create({"name": tag, \
                                                 "tag_name": tag, \
                                                 "description": release})
        return release

    def after_mr(self, commit=None):
        '''
        detect if we are after an MR

        Args:
            commits (string, optional): current commit sha
        Returns:
            bool: whether this commit is directly after an MR
        '''
        # check if this commit has an MR
        mr = self.get_mr(commit)
        if mr is not None:
            # we are after an mr
            return True
        # we are not after an mr
        return False

    def build_change_log(self, commit=None, prepend=None):
        '''
        builds change log from this release to the last release

        Args:
            commits (string, optional): current commit sha
        Returns:
            list: list of changes for change log
        '''
        # get our last tag
        last_release = self.get_last_tag(prepend, release=True)

        # change log
        change_log = []

        # check if this is our first release
        #if last_release is None:
        #    return change_log

        # get our mr
        mr = self.get_mr(commit, release=True)
        if mr is not None:
            # loop through commits until we hit the last release
            for mr_commit in mr.commits()._list:
                
                # add commits up to the last release
                if last_release is not None:
                    if mr_commit["id"] == last_release["commit"]["id"]:
                        break

                # only add commits that contain clog
                if "#clog" in mr_commit["message"]:
                    change_log.append("- {}\n  ".format(mr_commit["title"]))
        return change_log
