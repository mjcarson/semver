#!/usr/bin/env python3

'''automatic semantic versioning'''

import re
import os
import semver 
import argparse
import datetime
import subprocess

from utils import gapi

class Gensemver():
    '''automatic semantic versioning'''
    def __init__(self):
        '''inits semver''' 
        # get args
        self.args = _parse_args()

        # setup gitlab api client
        self.gapi = gapi.Gapi(self.args.url, token_type=self.args.token_type, token=self.args.token)

        # if we are on a already tagged
        self.already_tagged = False

    def bump_version(self, old, bump):
        '''
        bump to newest version based on commit
        Args:
            prever (string): previous version number
            bump (string): how to bump version number
        Returns:
            string: new version number
        '''
        # temporarily strip out prepend if its passed in
        if self.args.prepend is not None:
            old = old.replace(self.args.prepend, '')

        # if #major in commit message then bump major
        if "major" in bump.lower():
            new = semver.bump_major(old)

        # if #minor in commit message then bump minor
        elif "minor" in bump.lower():
            new = semver.bump_minor(old)
        
        # if #patch in commit message then bump patch
        elif "patch" in bump.lower():
            new = semver.bump_patch(old)

        # add prepend to new version if prepend is passed in
        if self.args.prepend is not None:
            new = self.args.prepend + new
        return new

    def get_version(self):
        '''
        Gets new version number or prints the current version number and exits

        Returns:
            string: version number
            None: no version number tied to this commit
        '''
        # check if this commit has already been tagged
        current_tag = self.gapi.get_tag()
        if current_tag is not None:
            # set already tagged flag
            self.already_tagged = True
            return current_tag

        # make sure this commit is after an MR
        if self.gapi.after_mr() == True:
            # get last version
            last_tag = self.gapi.get_last_tag(prepend=self.args.prepend)
            # use base tag if last tag is None
            if last_tag is None:
                last_tag= {}
                last_tag["name"] = self.args.base
            
            # get correct bump
            bump = self.gapi.get_bump()
            # bump version
            new = self.bump_version(last_tag["name"], bump)
            return new

    def _is_release(self):
        '''
        determines is this is a release branch

        Returns:
            bool: True if release branch, False if not
        '''
        # use specified release branches or just default to master
        if "RELEASE_BRANCHES" in os.environ.keys():
            # get list of release delimited by ', '
            release_branches = os.environ["RELEASE_BRANCHES"].split(', ')
        else:
            release_branches = ["master"]
        
        # get name of our branch
        current_branch = os.environ["CI_COMMIT_REF_NAME"]

        # check if our branch is a release branch
        if current_branch in release_branches:
            return True
        return False

    def _build_release_notes(self, version):
        '''
        builds release notes and a change log

        Args:
            version (string): version number of release
        Returns:
            string: built release notes
        '''
        # build release notes
        notes = []
        # use specified name or project name
        if self.args.name is None:
            if "CI_PROJECT_NAME" in os.environ:
                name = os.environ["CI_PROJECT_NAME"]
            else:
                name = ""
        # use specified name
        else:
            name = self.args.name
        notes.append("## {} {}\n".format(name, version))
        # date of release
        today = datetime.date.today()
        notes.append("##### {}\n  ".format(today.strftime("%B %d, %Y")))
        # build change log
        change_log = self.gapi.build_change_log(prepend=self.args.prepend)
        notes = notes + change_log

        # combine list of strings into one
        combined = "".join(notes)
        return combined

    def tag(self, version):
        '''
        tags our commit with new version

        Args:
            version (string): new version to tag commit with
        '''
        # get if this is a release or not
        if self._is_release() == True:
            # generate release notes
            release_notes = self._build_release_notes(version)
        else:
            release_notes = None

        # make a new tag if were not already tagged
        if self.already_tagged == False:
            # create tag
            tag = self.gapi.create_tag(version, release=release_notes)

        # were on an already tagged commit so just add release notes if there are any
        if release_notes is not None and self.already_tagged == True:
            self.gapi.inject_release_notes(version, release_notes)

    def start(self):
        # get new or current version number
        version = self.get_version()
    
        # if version is None then exit
        if version is None:
            exit()

        # skip tagging if skip tag is true
        if self.args.skiptag == False:
            # only make tag if we are the latest commit
            if self.gapi.latest() == True:
                self.tag(version)

        print(version)

def _parse_args():
    '''
    parses command line arguments
    '''
    # parse args
    parser = argparse.ArgumentParser(\
                description="automatic semantic versioning")
    parser.add_argument("--url", \
                        default="https://gitlab.com", \
                        help="url to gitlab instance")
    parser.add_argument("--name", \
                        help="Name of project for release notes")
    parser.add_argument("--base", \
                        default="0.0.0", \
                        help="base version to start at for repos with no \
                        previous versioning")
    parser.add_argument("--skiptag", \
                        default=False, \
                        action="store_true", \
                        help="Skip tagging and just return new version")
    parser.add_argument("--prepend", \
                        default=None,
                        help="string to prepend versions with")
    parser.add_argument("--token", \
                        help="token to use to talk to api")
    parser.add_argument("--token_type", \
                        default="job",
                        help="type of token to use to talk to api 'private' || 'job'")


    return parser.parse_args()

if __name__ == "__main__":
    gensemver = Gensemver()
    gensemver.start()
